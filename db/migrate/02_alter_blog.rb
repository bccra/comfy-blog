class AlterBlog < ActiveRecord::Migration

  def self.up
    add_column :comfy_blogs, :show_layout_id, :integer
    add_column :comfy_blogs, :index_layout_id, :integer
  end

  def self.down
    remove_column :comfy_blogs, :index_layout_id
    remove_column :comfy_blogs, :show_layout_id
  end

end