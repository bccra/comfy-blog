class LayoutTag
  include ComfortableMexicanSofa::Tag

  def self.regex_tag_signature(identifier = nil)    
    identifier ||= IDENTIFIER_REGEX
    /\{\{\s*cms:blog:(#{identifier})\s*\}\}/
  end

  def content
    case identifier
    when 'content'
      block.content
    when 'title'
      block.title
    when 'comments'
      "<%= render :partial => 'comfy/blog/comments/form' %>"
    when 'posts'
      "<%= render :partial => 'comfy/blog/posts/list' %>"
    when 'pagination'
      "<%= render :partial => 'comfy/blog/posts/pagination' %>"
    else
      ""
    end
  end

  def render
    content
  end

end