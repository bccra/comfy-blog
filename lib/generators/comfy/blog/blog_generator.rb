require 'rails/generators/active_record'

module Comfy
  module Generators
    class BlogGenerator < Rails::Generators::Base
      
      include Rails::Generators::Migration
      include Thor::Actions
      
      source_root File.expand_path('../../../../..', __FILE__)
      
      def self.next_migration_number(dirname)
        ActiveRecord::Generators::Base.next_migration_number(dirname)
      end

      def generate_migration
        copy_migration_file('create_blog', '01')
        copy_migration_file('alter_blog', '02')
      end
      
      
      def generate_initialization
        copy_file 'config/initializers/comfy_blog.rb',
          'config/initializers/comfy_blog.rb'
      end
      
      def generate_routing
        route_string  = "  comfy_route :blog_admin, :path => '/admin'\n"
        route_string << "  comfy_route :blog, :path => '/blog'\n"
        route route_string[2..-1]
      end
      
      def generate_views
        directory 'app/views/comfy/blog', 'app/views/comfy/blog'
      end
      
      def show_readme
        readme 'lib/generators/comfy/blog/README'
      end

      private
      
        def copy_migration_file(name, orig_index)
          destination   = File.expand_path("db/migrate/#{orig_index}_#{name}.rb", self.destination_root)
          migration_dir = File.dirname(destination)
          destination   = self.class.migration_exists?(migration_dir, name)
          
          if destination
            puts "\e[0m\e[31mFound existing #{name} migration. Remove it if you want to regenerate.\e[0m"
          else
            migration_template "db/migrate/#{orig_index}_#{name}.rb", "db/migrate/#{name}.rb"
          end
        end
      
    end
  end
end