class Comfy::Blog::Blog < ActiveRecord::Base
  
  self.table_name = 'comfy_blogs'
  
  # -- Relationhips ---------------------------------------------------------
  belongs_to :site, :class_name => 'Comfy::Cms::Site'
  belongs_to :show_layout, :class_name => 'Comfy::Cms::Layout'
  belongs_to :index_layout, :class_name => 'Comfy::Cms::Layout'
  
  has_many :posts,
    :dependent  => :destroy
  has_many :comments,
    :through    => :posts
    
  # -- Validations ----------------------------------------------------------
  validates :site_id, :label, :identifier,
    :presence   => true
  validates :identifier,
    :format     => { :with => /\A\w[a-z0-9_-]*\z/i }
  validates :path,
    :uniqueness => { :scope => :site_id },
    :format     => { :with => /\A\w[a-z0-9_-]*\z/i },
    :presence   => true,
    :if         => 'restricted_path?'

  def blockable_object
    OpenStruct.new(:tags => [], :blocks => [])
  end

  def render_index    
    ComfortableMexicanSofa::Tag.process_content(blockable_object, self.index_layout.merged_content)
  end
  
protected

  def restricted_path?
    (self.class.count > 1 && self.persisted?) ||
    (self.class.count >= 1 && self.new_record?)
  end

end